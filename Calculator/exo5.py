# Objectif :
# Create a class for :
# sum, substract, multiply et divide between 2 integer
# Handle interger input and division by zero



class SimpleCalculator:
    """
    Will calculate the operation of 2 variables
    """

    def __init__(self, init1, init2):
        """ 
        Init function 
        """
        if (isinstance(init1, int) and isinstance(init2, int)) == False:
            raise ValueError("Need integers input")
        self.chiffre1 = init1
        self.chiffre2 = init2

    def sum(self):
        """ 
        addition function
        """

        print(self.chiffre1, "+", self.chiffre2, "=")
        res = self.chiffre1 + self.chiffre2
        return res

    def substract(self):
        """ 
        substract function
        """
        print(self.chiffre1, "-", self.chiffre2, "=")
        res = self.chiffre1 - self.chiffre2
        return res

    def multiply(self):
        """ 
        multiply function
        """
        print(self.chiffre1, "*", self.chiffre2, "=")
        res = self.chiffre1 * self.chiffre2
        return res

    def divide(self):
        """
        divide function
        """
        if self.chiffre2 == 0:
            raise ZeroDivisionError("Cannot divide by zero")

        print(self.chiffre1, "/", self.chiffre2, "=")
        res = self.chiffre1 / self.chiffre2
        return res


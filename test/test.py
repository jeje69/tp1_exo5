# Objectif :
# Create a class for :
# unitary test

from Calculator.ex5 import Simple_calculator

job = Simple_calculator(3,2)
print(job.sum())
print(job.substract())
print(job.multiply())
print(job.divide())

job = Simple_calculator(3,1.2)
print(job.sum())
print(job.substract())
print(job.multiply())
print(job.divide())

job = Simple_calculator(3.3,1)
print(job.sum())
print(job.substract())
print(job.multiply())
print(job.divide())

job = Simple_calculator(3,0)
print(job.sum())
print(job.substract())
print(job.multiply())
print(job.divide())

